terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-842024-tfstate"
    key            = "recipe-app-api-devops.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state.lock"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.37.0"
    }
  }
}

provider "aws" {
  region  = "us-west-1"
  profile = "Lawrence_Cymbura"
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}