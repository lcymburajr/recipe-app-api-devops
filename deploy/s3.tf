resource "aws_s3_bucket" "app_public_files" {
  bucket_prefix = "${local.prefix}-files-1234fsadf"
  force_destroy = true
}