#!/bin/bash

sudo yum update -y
sudo yum install docker -y
sudo systemctl enable docker.service
sudo usermod -aG docker ec2-user
sudo systemctl start docker.service
